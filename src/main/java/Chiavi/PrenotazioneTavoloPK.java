package Chiavi;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;


public class PrenotazioneTavoloPK implements Serializable {
	
	private static final long serialVersionUID = 9010721175630108492L;

	Integer numTavolo;
	
	LocalDate data;
	
	
	public Integer getNumTavolo() {
		return numTavolo;
	}

	public void setNumTavolo(Integer numTavolo) {
		this.numTavolo = numTavolo;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
	
	
	public PrenotazioneTavoloPK(){}
	 
	
	public boolean equals(Object oggetto)
	{
	if (oggetto instanceof PrenotazioneTavoloPK)
	{
		PrenotazioneTavoloPK altrachiave = (PrenotazioneTavoloPK)oggetto;
	return (altrachiave.numTavolo.equals(numTavolo) && altrachiave.data.equals(data));
	}
	return false;
	}
	 
	public int hashCode()
	{
		return super.hashCode();
	}
	
}