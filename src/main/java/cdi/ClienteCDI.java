package cdi;

import javax.inject.Named;
import javax.servlet.http.HttpSession;

import Ejb.UserDao;
import Entity.Cliente;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import java.io.Serializable;
import java.util.ArrayList;

@Named
@SessionScoped
public class ClienteCDI implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private String outcome;
	
	private HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	
	@EJB
	private UserDao ud;
	
	private Cliente c;
	
	
	@PostConstruct
	public void init() {
		
		c=new Cliente();
		
	}
	
	
	public String crea() {
		
		if(checkRegistrazione()) {
			saveSession();
		}
		
		if(!outcome.equals("registrazione.xhtml"))	ud.create(c);
		
		
		return outcome;
		
	}
	
	private boolean checkRegistrazione() {
		
		Cliente cDB = null;
		
		cDB = (Cliente) ud.findById(c.getUsername());
		
		if(cDB == null) {
			
			outcome="loginC";
			return true;
			
		}else {//Utente presente
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Errore: Username già presente", "");
			facesContext.addMessage(null, facesMessage);
			
			outcome="registrazione.xhtml";
			
		}
		return false;
		
	}



	
	
	public String login() {
		
		if(checkLogin()) {
			saveSession();
		}
		
		return outcome;
	}

	private boolean checkLogin(){
		Cliente cDB = null;
		
		cDB = (Cliente) ud.findById(c.getUsername());
		
		if(cDB == null || !c.getPassword().equals(cDB.getPassword())) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Errore: Username e/o Password errati", "");
			facesContext.addMessage(null, facesMessage);
			return false;
		}else {//Utente presente
			if(c.getUsername().equals("admin") && c.getPassword().equals(cDB.getPassword())) {
				outcome="loginA";
				return true;
			}
			else if(c.getUsername().equals(cDB.getUsername()) && c.getPassword().equals(cDB.getPassword())) {
				outcome="loginC";
				return true;
			}
		}
		
		outcome="login";
		return false;
	}
	
	

	
	private void saveSession() {
		
		session.setAttribute("username", c.getUsername());
		
	}
	
	public String getSession() {
		return (String) session.getAttribute("username");
	}
	
	
	public Cliente getCliente() {
		System.out.println("clienteget");
		return c;
	}
	
	public void setCliente(Cliente c) {
		System.out.println("clienteset");
		this.c=c;
	}
	
	public String logout() {
		session.invalidate();
		return "login.xhtml";
	}
	
	public String goHome() {
		
		String outcome="login.xhtml";
		
		if(getSession() != null) {
			if(getSession().equals("admin")) outcome="indexA.xhtml";
			else outcome="indexC.html";
		}
		
		return outcome;
		
	}
	
	
	
	
}