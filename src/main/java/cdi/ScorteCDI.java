package cdi;

import javax.inject.Named;

import Ejb.ScorteDao;
import Entity.Cliente;
import Entity.Scorte;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.PostActivate;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;

import java.io.Serializable;
import java.util.ArrayList;

@Named
@RequestScoped
public class ScorteCDI implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	//Numero di elementi che verranno inseriti in listScorte e, quindi, visualizzati
	private final int elemOutput=8;
	
	
	private Scorte s;
	
	@EJB
	private ScorteDao sd;
	
	private ArrayList<Scorte> listScorte=null;
	
	
	@PostConstruct
	public void init() {
		
		listScorte = new ArrayList<Scorte>();
		s = new Scorte();
		
		//Inserisce in listScorte un numero di elementi pari a elemOutput
		selezionaElemOutput();	
		
	}
	

	public String preparaCreazione() {
		listScorte = new ArrayList<Scorte>();
		s = new Scorte();
		
		//Inserisce in listScorte un numero di elementi pari a elemOutput
		selezionaElemOutput();	
		
		return "inserisciScorta";
	}
	
	public String crea() {
		
		if(s.getQta()==null) s.setQta(0);
		
		for(Scorte sTemp: listScorte) {
			
			if(sTemp.getNome().toLowerCase().equals(s.getNome().toLowerCase())) {
				sTemp.setQta(s.getQta()+sTemp.getQta());
				sd.update(sTemp);
				return "index";
			}
			
		}
		
		sd.create(s);
		
		
		
		listScorte.add(s);
		
		return "index";
		
	}
	
	public String delete() {
		
		for(Scorte sTemp: listScorte) {
			
			if(sTemp.getNome().toLowerCase().equals(s.getNome().toLowerCase())) {
				sd.deleteById(sTemp.getCodiceS());
				listScorte.remove(sTemp);
				return "index";
			}
			
		}
		
		return "index";
		
	}
	
	
	public Scorte getScorta() {
		return s;
	}
	
	public void setScorta(Scorte s) {
		//Controlli
		this.s=s;
	}
	
	
	
	public void selezionaElemOutput() {
		
		listScorte.addAll((ArrayList<Scorte>)sd.listAll(0));
		
		int lunghezza = listScorte.size();
		if(lunghezza >= 8) 
			for(int i=0; i<lunghezza - elemOutput; i++) 
				listScorte.remove(0);
			
		setScorte( listScorte );

	}
	
	public ArrayList<Scorte> getScorte() {
		
		return listScorte;
		
	}

	public void setScorte(ArrayList<Scorte> listScorte) {
		this.listScorte = listScorte;
	}
	
}