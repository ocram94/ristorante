package cdi;

import javax.inject.Named;
import javax.servlet.http.HttpSession;

import Ejb.PrenotazioneTavoloDao;
import Ejb.UserDao;
import Entity.Cliente;
import Entity.PrenotazioneTavolo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

@Named
@RequestScoped
public class PrenotazioneTavoloCDI implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String outcome;
	
	@EJB
	private PrenotazioneTavoloDao pd;
	
	@EJB
	private UserDao cliente;
	
	private PrenotazioneTavolo pt;
	private Cliente c;
	
	private HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	
	
	@PostConstruct
	public void init() {
		LocalDate d=LocalDate.now();
		pt=new PrenotazioneTavolo();
		pt.setCliente((Cliente)cliente.findById((String)session.getAttribute("username")));
		pt.setNumTavolo(1003);
		System.out.println("cazzoinit"+pt.getNumTavolo());
		//pt.setCliente((Cliente)cliente.findById("admin"));
		
		pt.setNumPersone(1003);

		
	}
	
	
	public String crea() {
		System.out.println("cazzoCrea"+pt.getData());
		pt.setData(pt.getData());
		pd.create(pt);
		
		outcome="prenotazioneAmministratore.xhtml";
		
		return outcome;
		
	}
	
	
	public PrenotazioneTavolo getPrenotazioneTavolo() {
		return pt;
	}
	
	public void setPrenotazioneTavolo(PrenotazioneTavolo pt) {
		this.pt=pt;
	}
	
	public String minData() {

		String mese="0";
		String giorno="0";
		
		//LocalDateTime d=LocalDateTime.of(LocalDate.now(),LocalTime.parse("12:00"));
		LocalDate d=LocalDate.now();
		
		if(d.getMonthValue()<=9) {
			mese="0"+d.getMonthValue();
		}else {
			mese=""+d.getMonthValue();
		}
		if(d.getDayOfMonth()<=9) {
			giorno="0"+d.getDayOfMonth();
		}else {
			giorno=""+d.getDayOfMonth();
		}
		
		return ""+d.getYear()+"-"+mese+"-"+giorno;
		
	}
	
	
	public LocalDate getData() {
		System.out.println("cazzogetData"+pt.getData());
		LocalDate d=pt.getData();
		//Date data=Date.from(d.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				
		return d;
	}
	
	
	public void setData(Date d) {
		System.out.println("cazzoset"+d);
		//pt.setData(d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		//pt.setData(d);
	}
	
	public Cliente getCliente() {
		return pt.getCliente();
	}
	
	public void setCliente(Cliente c) {
		pt.setCliente(c);
	}
	
	public int getNumPersone() {
		return pt.getNumPersone();
	}
	
	public void setNumPersone(int num) {
		pt.setNumPersone(num);
	}
	
	public String getUsername() {
		return "";
	}
	
	
	public void setUsername(String username) {
		Cliente c=new Cliente();
		c=(Cliente)cliente.findById(username);
		
		if(c==null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Errore: Username non presente", "");
			facesContext.addMessage(null, facesMessage);
			
		}else {
			pt.setCliente(c);
		}
		
	}
	
	

	/*
	private boolean checkPrenotazione() {
		
		List<PrenotazioneTavolo> ptDB = new LinkedList<>();
		
		ptDB = pd.findById(pt.getData());
		
		
		if(ptDB != null) {
			
			for(PrenotazioneTavolo pt: ptDB) {
				if(pt.get)
			}
			outcome="loginC";
			return true;
			
		}else {//Utente presente
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Errore: Username già presente", "");
			facesContext.addMessage(null, facesMessage);
			
			outcome="registrazione.xhtml";
			
		}
		return false;
		
	}
	*/
	
	
	
	
}