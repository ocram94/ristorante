package Servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import Ejb.ScorteDao;
import Ejb.UserDao;
import Entity.Amministratore;
import Entity.Cliente;
import Entity.Ingredienti;
import Entity.Ordine;
import Entity.Pietanza;
import Entity.PrenotazioneTavolo;
import Entity.Scorte;
import Entity.User;
import cdi.ScorteCDI;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


@WebServlet(urlPatterns = "cRUD")
public class CRUD extends HttpServlet {

	@PersistenceContext
	EntityManager em;
	
	@Resource
	UserTransaction ux;
	
	@EJB
	private ScorteDao ud;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().println("Method doGet invoked");
		
		
		try {
			
			
			/*
			ux.begin();//inizio transazione
			
			Ingredienti ingredienti =new Ingredienti();
			ingredienti.setNome("rz");
			ingredienti.setQta(100);
			em.persist(ingredienti);
			
			ux.commit();//fine transazione
			
			
			ux.begin();
			
			Pietanza pietanza =new Pietanza();
			pietanza.setId("zitt");
			pietanza.setCosto(54);
			em.persist(pietanza);
			
			ux.commit();
			
			ux.begin();
			*/
		
			
			
			
			/*
			ux.begin();//Inizio transazione
			Cliente cliente =new Cliente();
			cliente.setNome("Marco");
			cliente.setCognome("Minervino");
			cliente.setMail("miaMail");
			cliente.setUsername("userMarco");
			cliente.setPassword("miaPassword");
			cliente.setIndirizzo("Via dfsd");
			em.persist(cliente);
			ux.commit();//Fine transazione
			*/
			
			/*
			ux.begin();//Inizio transazione
			Amministratore a =new Amministratore();
			a.setNome("Marco");
			a.setCognome("Minervino");
			a.setMail("miaMail");
			a.setUsername("userMarcox");
			a.setPassword("miaPassword");
			a.setPrenotazioneTavolo(true);
			a.setOrdine(true);
			a.setMenu(true);
			a.setMagazzino(true);
			em.persist(a);
			ux.commit();//Fine transazione
			*/
			
			/*
			ux.begin();//Inizio transazione
			PrenotazioneTavolo pt =new PrenotazioneTavolo();
			pt.setNumTavolo(5);
			LocalDateTime date=LocalDateTime.of(LocalDate.of(2018,  Month.DECEMBER, 18),LocalTime.parse("10:15:00"));
			
			
			
			pt.setData(date);
			em.persist(pt);
			ux.commit();//Fine transazione
			*/
			
			/*
			ux.begin();
			Ordine o=new Ordine();
			LocalDateTime d=LocalDateTime.of(LocalDate.of(2018, Month.JULY, 13), LocalTime.parse("10:30:00"));
			
			o.setData(d);
			em.persist(o);
			ux.commit();
			*/
			/*
			ux.begin();//Inizio transazione
			
			Cliente cliente = new Cliente();
			cliente.setNome("Marco");
			cliente.setCognome("Minervino");
			cliente.setMail("miaMail");
			cliente.setUsername("zzzzz");
			cliente.setPassword("miaPassword");
			cliente.setIndirizzo("Via dfsd");
			
			
			
			//PrenotazioneTavolo pr=new PrenotazioneTavolo();
			LocalDateTime d=LocalDateTime.of(LocalDate.of(2018, Month.JULY, 22), LocalTime.parse("10:30"));
			
			//pr.setData(d);
			//pr.setNumTavolo(5);
			
			//pr.setCliente(cliente);
			
			Ordine o=new Ordine();
			o.setData(d);
			o.setCliente(cliente);
			
			Pietanza p=new Pietanza();
			p.setId("swdefr");
			p.setCosto(15);
			p.addOrdine(o);
			
			Ingredienti i = new Ingredienti();
			i.setCodice(1l);
			i.setNome("ravioli");
			i.setQta(50);
			
			Ingredienti i2=new Ingredienti();
			i2.setCodice(2l);
			i2.setNome("caccca");
			i2.setQta(888);
			
			i2.addPietanze(p);
			i.addPietanze(p);
			
			em.persist(i);
			em.persist(i2);
			
			p.addIngredienti(i);
			p.addIngredienti(i2);
			em.persist(p);
			
			o.addPietanze(p);
			
			em.persist(o);
			
			//em.persist(pr);
			
			//cliente.addPrenotazioneTavolo(pr);
			
			cliente.addOrdine(o);
			
			em.persist(cliente);
			
			ux.commit();
			*/
			
			/*
			Scorte sugo=new Scorte();
			sugo.setQta(500);
			sugo.setNome("Sugo");
			
			Scorte sale = new Scorte();
			sale.setQta(100);
			sale.setNome("sale");
			em.persist(sugo);
			em.persist(sale);
			
			Ingredienti i = new Ingredienti();
			i.setCodice(sugo.getCodiceS());
			i.setNome(sugo.getNome());
			i.setQta(50);
			
			Ingredienti i2=new Ingredienti();
			i2.setCodice(sale.getCodiceS());
			i2.setNome(sale.getNome());
			i2.setQta(888);
			
			
			em.persist(i);
			em.persist(i2);
			
			ux.commit();//Fine transazione
			*/
			
			
			
			
			
			ux.begin();
			/*
			Cliente cliente = new Cliente();
			cliente.setNome("Marco");
			cliente.setCognome("Minervino");
			cliente.setMail("miaMail");
			cliente.setUsername("666666666666666");
			cliente.setPassword("miaPassword");
			cliente.setIndirizzo("Via dfsd");
			em.persist(cliente);
			
			LocalDateTime d=LocalDateTime.of(LocalDate.of(2018, Month.JULY, 22), LocalTime.parse("10:30"));
			
			PrenotazioneTavolo pt = new PrenotazioneTavolo();
			pt.setCliente(cliente);
			pt.setData(d);
			pt.setNumPersone(5);
			pt.setNumTavolo(11);
			
			em.persist(pt);
			*/
			ux.commit();
			
			
			//em.persist(cliente);
			
			
			
			
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicMixedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicRollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		
		/*
		Scorte s=new Scorte();
		s.setNome("s1");
		s.setQta(25);
		
		ud.create(s);
		*/
		
	}

	@Override
	protected void doPost(javax.servlet.http.HttpServletRequest request,
			javax.servlet.http.HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().println("Method doPost invoked");
	}
}