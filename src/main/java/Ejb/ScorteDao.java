package Ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import Entity.Scorte;

/**
 * DAO for Scorte
 */
@Stateless
public class ScorteDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;

	public void create(Scorte entity) {
		em.persist(entity);
	}

	public void deleteById(Long id) {
		Scorte entity = em.find(Scorte.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Scorte findById(Long id) {
		return em.find(Scorte.class, id);
	}

	public Scorte update(Scorte entity) {
		return em.merge(entity);
	}

	public List<Scorte> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Scorte> findAllQuery = em.createQuery(
				"SELECT s FROM Scorte s",
				Scorte.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
	
	//"SELECT DISTINCT s FROM Scorte s ORDER BY s.codiceS"
	public List<Scorte> listAll(Integer startPosition) {
		TypedQuery<Scorte> findAllQuery = em.createQuery(
				"SELECT DISTINCT s FROM Scorte s ORDER BY s.codiceS",
				Scorte.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		return findAllQuery.getResultList();
	}
}
