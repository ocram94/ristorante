package Ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import Entity.Ingredienti;

/**
 * DAO for Ingredienti
 */
@Stateless
public class IngredientiDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;

	public void create(Ingredienti entity) {
		em.persist(entity);
	}

	public void deleteById(Long id) {
		Ingredienti entity = em.find(Ingredienti.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Ingredienti findById(Long id) {
		return em.find(Ingredienti.class, id);
	}

	public Ingredienti update(Ingredienti entity) {
		return em.merge(entity);
	}

	public List<Ingredienti> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Ingredienti> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT i FROM Ingredienti i LEFT JOIN FETCH i.pietanze ORDER BY i.codice",
						Ingredienti.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}
