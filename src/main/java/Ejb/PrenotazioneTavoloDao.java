package Ejb;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import Entity.PrenotazioneTavolo;

/**
 * DAO for PrenotazioneTavolo
 */
@Stateless
public class PrenotazioneTavoloDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;

	public void create(PrenotazioneTavolo entity) {
		em.persist(entity);
	}

	public void deleteById(LocalDate id) {
		PrenotazioneTavolo entity = em.find(PrenotazioneTavolo.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public List<PrenotazioneTavolo> findById(LocalDate id) {
		TypedQuery<PrenotazioneTavolo> prenotazioni = em
				.createQuery(
						"SELECT p FROM PrenotazioneTavolo p WHERE p.data==id",
						PrenotazioneTavolo.class);
		return prenotazioni.getResultList();
	}

	public PrenotazioneTavolo update(PrenotazioneTavolo entity) {
		return em.merge(entity);
	}

	public List<PrenotazioneTavolo> listAll(Integer startPosition,
			Integer maxResult) {
		TypedQuery<PrenotazioneTavolo> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT p FROM PrenotazioneTavolo p LEFT JOIN FETCH p.cliente ORDER BY p.numTavoloORDER BY p.data",
						PrenotazioneTavolo.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}
