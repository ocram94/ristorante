package Ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import Entity.Pietanza;

/**
 * DAO for Pietanza
 */
@Stateless
public class PietanzaDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;

	public void create(Pietanza entity) {
		em.persist(entity);
	}

	public void deleteById(String id) {
		Pietanza entity = em.find(Pietanza.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Pietanza findById(String id) {
		return em.find(Pietanza.class, id);
	}

	public Pietanza update(Pietanza entity) {
		return em.merge(entity);
	}

	public List<Pietanza> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Pietanza> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT p FROM Pietanza p LEFT JOIN FETCH p.ordine LEFT JOIN FETCH p.ingredienti ORDER BY p.nome",
						Pietanza.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}
