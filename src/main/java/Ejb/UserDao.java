package Ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import Entity.User;

/**
 * DAO for User
 */
@Stateless
public class UserDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;
	

	public void create(User entity) {
		em.persist(entity);
	}

	public void deleteById(String id) {
		User entity = em.find(User.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public User findById(String id) {
		User u=null;
		try {
			u = em.find(User.class, id);
		}catch(Exception e) {
			System.out.println("Utente non trovato");
		}
		return u;
		
	}
	
	public User update(User entity) {
		return em.merge(entity);
	}

	public List<User> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<User> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT u FROM User u ORDER BY u.username",
						User.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}
