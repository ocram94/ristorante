package Ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import Entity.Ordine;

/**
 * DAO for Ordine
 */
@Stateless
public class OrdineDao {
	@PersistenceContext(unitName = "ProgettoRistorante-persistence-unit")
	private EntityManager em;

	public void create(Ordine entity) {
		em.persist(entity);
	}

	public void deleteById(Long id) {
		Ordine entity = em.find(Ordine.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Ordine findById(Long id) {
		return em.find(Ordine.class, id);
	}

	public Ordine update(Ordine entity) {
		return em.merge(entity);
	}

	public List<Ordine> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Ordine> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT o FROM Ordine o LEFT JOIN FETCH o.cliente LEFT JOIN FETCH o.pietanze ORDER BY o.id",
						Ordine.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}
