package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Version;

@Entity
@Table(name = "cliente")
@DiscriminatorValue(value="C")
@PrimaryKeyJoinColumn(name="username")
public class Cliente extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;

	@Column(name="indirizzo")
	private String indirizzo;
	
	@OneToMany(mappedBy="cliente")
	protected List<PrenotazioneTavolo> prenotazioneTavolo=new LinkedList<PrenotazioneTavolo>();
	
	@OneToMany(mappedBy="cliente")
	protected List<Ordine> ordine=new LinkedList<Ordine>();
	
	
	public List<Ordine> getOrdine(){
		return ordine;
	}
	
	
	public void setOrdine(List<Ordine> o) {
		this.ordine=o;
	}
	
	public void addOrdine(Ordine o) {
		this.ordine.add(o);
	}
	
	public String getIndirizzo() {
		return this.indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		System.out.println("indirizzodelcazzo");
		this.indirizzo = indirizzo;
	} 
	
	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public List<PrenotazioneTavolo> getPrenotazioneTavolo(){
		return this.prenotazioneTavolo;
	}
	
	
	public void setPrenotazioneTavolo(List<PrenotazioneTavolo> pr) {
		this.prenotazioneTavolo=pr;
	}
	
	public void addPrenotazioneTavolo(PrenotazioneTavolo pr) {
		this.prenotazioneTavolo.add(pr);
	}
	
	public boolean removePrenotazioneTavolo(PrenotazioneTavolo pr) {
		try {
			prenotazioneTavolo.remove(pr);
			return true;
		}catch(Exception e) {
			
		}
		return false;
	}
	
	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (username != null)
			result += "username: " + username;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Cliente)) {
			return false;
		}
		Cliente other = (Cliente) obj;
		if (username != null) {
			if (!username.equals(other.username)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	
}