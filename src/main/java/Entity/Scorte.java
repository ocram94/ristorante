package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;

@Entity
@Table(name = "scorte")
public class Scorte implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codiceS", updatable = false, nullable = false)
	private Long codiceS;
	@Version
	@Column(name = "version")
	private int version;

	@Column(name = "qta")
	protected Integer qta;
	
	@Column(name = "nome")
	protected String nome;
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getQta() {
		return qta;
	}

	public void setQta(Integer Qta) {
		this.qta = Qta;
	}
	
	public Long getCodiceS() {
		return this.codiceS;
	}

	public void setCodiceS(final Long id) {
		this.codiceS = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (codiceS != null)
			result += "id: " + codiceS;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Scorte)) {
			return false;
		}
		Scorte other = (Scorte) obj;
		if (codiceS != null) {
			if (!codiceS.equals(other.codiceS)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codiceS == null) ? 0 : codiceS.hashCode());
		return result;
	}
}