package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.Version;

import Chiavi.PrenotazioneTavoloPK;

@Entity
@Table(name = "prenotazioneTavolo")
@IdClass(PrenotazioneTavoloPK.class)
public class PrenotazioneTavolo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;
	
	@Id
	protected Integer numTavolo;
	
	@Id
	protected LocalDate data;
    
	@Column(name = "numPersone")
	protected int numPersone;
	
	@ManyToOne
	@JoinColumn(name="username_cliente", referencedColumnName="username")
	protected Cliente cliente;
	
	
	public PrenotazioneTavolo() {}
	
	
	public void setNumPersone(int numero) {
		this.numPersone= numero;
	}
	
	public int getNumPersone() {
		return this.numPersone;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public void setCliente(Cliente c) {
		this.cliente=c;
	}
	
	public Integer getNumTavolo() {
		System.out.println("cazzoget"+this.numTavolo);
		return this.numTavolo;
	}

	public void setNumTavolo(Integer numTavolo) {
		this.numTavolo = numTavolo;
		System.out.println("cazzoset"+this.numTavolo);
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	
	
	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (numTavolo != null && data != null)
			result += "numTavolo: " + numTavolo + " data: " + data;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PrenotazioneTavolo)) {
			return false;
		}
		PrenotazioneTavolo other = (PrenotazioneTavolo) obj;
		if (numTavolo != null && data != null) {
			if ( !(numTavolo.equals(other.numTavolo) && (data.equals(other.data)) ) ) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numTavolo == null) ? 0 : numTavolo.hashCode());
		return result;
	}
	
}