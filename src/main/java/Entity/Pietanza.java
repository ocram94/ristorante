package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Column;
import javax.persistence.Version;

@Entity
@Table(name = "pietanza")
public class Pietanza implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;
	
	@Id
	//@Column(name = "nome", updatable = false, nullable = false, columnDefinition="character varying(255)")
	@Column(name = "nome", updatable = false, nullable = false)
	private String nome;
	
	@Column(name = "costo")
	private Integer costo;
	
	
	@ManyToMany(mappedBy="pietanze")
	protected List<Ordine> ordine = new LinkedList<Ordine>();
	
	@ManyToMany
	@JoinTable(name="ingredienti_pietanza",joinColumns=@JoinColumn(name="nome_pietanza", 
	referencedColumnName="nome"),inverseJoinColumns=@JoinColumn(name="codice_ingrediente", 
	referencedColumnName="codice"))
	protected List<Ingredienti> ingredienti = new LinkedList<Ingredienti>();
	
	public List<Ingredienti> getIngredienti(){
		return ingredienti;
	}
	
	public void setIngredienti(List<Ingredienti> i) {
		this.ingredienti=i;
	}
	
	public void addIngredienti(Ingredienti i) {
		this.ingredienti.add(i);
	}
	
	public List<Ordine> getOrdine(){
		return ordine;
	}
	
	public void setOrdine(List<Ordine> o) {
		this.ordine=o;
	}
	
	public void addOrdine(Ordine o) {
		this.ordine.add(o);
	}
	
	public String getId() {
		return this.nome;
	}

	public void setId(final String nome) {
		this.nome = nome;
	}
	
	public Integer getCosto() {
		return costo;
	}

	public void setCosto(Integer costo) {
		this.costo = costo;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Pietanza)) {
			return false;
		}
		Pietanza other = (Pietanza) obj;
		if (nome != null) {
			if (!nome.equals(other.nome)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (nome != null)
			result += "Codice: " + nome;
		if (costo != null)
			result += ", Qta: " + costo;
		return result;
	}
	
}