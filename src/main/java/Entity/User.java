package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Version;

@Entity
@Table(name = "utente")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="TIPO_UTENTE", discriminatorType=DiscriminatorType.STRING, length=1)
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;
	
	@Id
	@Column(name = "username", updatable = false, nullable = false, columnDefinition="character varying(255)")
	protected String username;
	
	@Column(name = "nome")
	private String nome;

	@Column(name = "cognome")
	private String cognome;

	@Column(name = "mail")
	private String mail;

	@Column(name = "password")
	private String password;
	

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (username != "") {
			if (!username.equals(other.username)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == "") ? 0 : username.hashCode());
		return result;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (nome != null && !nome.trim().isEmpty())
			result += "nome: " + nome;
		if (cognome != null && !cognome.trim().isEmpty())
			result += ", cognome: " + cognome;
		if (mail != null && !mail.trim().isEmpty())
			result += ", mail: " + mail;
		if (password != null && !password.trim().isEmpty())
			result += ", password: " + password;
		if (username != null && !username.trim().isEmpty())
			result += ", username: " + username;
		return result;
	}
	
}