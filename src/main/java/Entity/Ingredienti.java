package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;

@Entity
@Table(name = "Ingredienti")
public class Ingredienti implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codice", updatable = false, nullable = false)
	private Long codice;
	
	@Column(name = "qta")
	private Integer qta;
	
	@Column(name = "nome")
	private String nome;
	
	@ManyToMany(mappedBy="ingredienti")
	protected List<Pietanza> pietanze = new LinkedList<Pietanza>();
	
	
	
	public List<Pietanza> getPietanze(){
		return pietanze;
	}
	
	public void setPietanze(List<Pietanza> p) {
		this.pietanze=p;
	}
	
	public void addPietanze(Pietanza p) {
		this.pietanze.add(p);
	}	
	
	
	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}
	
	public Long getCodice() {
		return codice;
	}

	public void setCodice(Long codice) {
		this.codice = codice;
	}

	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	 
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Ingredienti)) {
			return false;
		}
		Ingredienti other = (Ingredienti) obj;
		if (codice != null) {
			if (!codice.equals(other.codice)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codice == null) ? 0 : codice.hashCode());
		return result;
	}

	public Integer getQta() {
		return qta;
	}

	public void setQta(Integer Qta) {
		this.qta = Qta;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (codice != null)
			result += "Codice: " + codice;
		if (qta != null)
			result += ", Qta: " + qta;
		return result;
	}
	
}