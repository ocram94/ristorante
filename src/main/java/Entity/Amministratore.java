package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Version;

@Entity
@Table(name = "amministratore")
@DiscriminatorValue(value="A")
@PrimaryKeyJoinColumn(name="username")
public class Amministratore extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;

	@Column(name="prenotazioneTavolo")
	private boolean prenotazioneTavolo;
	
	@Column(name="ordine")
	private boolean ordine;
	
	@Column(name="magazzino")
	private boolean magazzino;
	
	@Column(name="menu")
	private boolean menu;
	
	
	public boolean isPrenotazioneTavolo() {
		return prenotazioneTavolo;
	}

	public void setPrenotazioneTavolo(boolean prenotazioneTavolo) {
		this.prenotazioneTavolo = prenotazioneTavolo;
	}

	public boolean isOrdine() {
		return ordine;
	}

	public void setOrdine(boolean ordine) {
		this.ordine = ordine;
	}

	public boolean isMagazzino() {
		return magazzino;
	}

	public void setMagazzino(boolean magazzino) {
		this.magazzino = magazzino;
	}

	public boolean isMenu() {
		return menu;
	}

	public void setMenu(boolean menu) {
		this.menu = menu;
	}

	public String getUsername() {
		return this.username;
	}

	public void setId(String username) {
		this.username = username;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	
	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (username != null)
			result += "id: " + username;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Amministratore)) {
			return false;
		}
		Amministratore other = (Amministratore) obj;
		if (username != null) {
			if (!username.equals(other.username)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	
}