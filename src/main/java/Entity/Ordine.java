package Entity;

import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;

@Entity
@Table(name = "ordine")
public class Ordine implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "version")
	private int version;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	
	LocalDateTime data;
	
	@ManyToOne
	@JoinColumn(name="username_cliente", referencedColumnName="username")
	protected Cliente cliente;
	
	
	@ManyToMany
	@JoinTable(name="ordine_pietanza",joinColumns=@JoinColumn(name="id_ordine", 
	referencedColumnName="id"),inverseJoinColumns=@JoinColumn(name="nome_pietanza", 
	referencedColumnName="nome"))
	protected List<Pietanza> pietanze = new LinkedList<Pietanza>();
	
	
	public List<Pietanza> getPietanze(){
		return pietanze;
	}
	
	public void setPietanze(List<Pietanza> p) {
		this.pietanze=p;
	}
	
	public void addPietanze(Pietanza p) {
		this.pietanze.add(p);
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente c) {
		this.cliente=c;
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}
	
	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	
	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (id != null)
			result += "id: " + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Ordine)) {
			return false;
		}
		Ordine other = (Ordine) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
}